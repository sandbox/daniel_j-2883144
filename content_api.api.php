<?php
/**
 * @file
 * Hooks provided by the Content API module.
 */

/**
 * Alter Content API node output.
 *
 * @param stdClass $node
 *   The sanitized node that is being output.
 * @param stdClass $raw_node
 *   The actual Drupal node from which data should be taken.
 * @param array $hidden_fields
 *   List of field names which should be excluded from output.
 * @param bool $show_fields
 *   Flag indicating whether fields should be displayed.
 */
function hook_content_api_alter(&$node, $raw_node, array $hidden_fields, $show_fields) {
  if (!in_array('body', $hidden_fields) && empty($node->body) && !empty($raw_node->body[LANGUAGE_NONE])) {
    $node->body = $raw_node->body[LANGUAGE_NONE];
  }
}
