Since the contentapi module appears to be abandoned, this is a Drupal 7 fork
that addresses code quality, extensibility, and security issues.

Note that this module's shortname is content_api with an underscore, to
differentiate it from the old contentapi module. When you enable the
content_api module, if the variables pertaining to the old contentapi module
are present, they will be ported over into the content_api namespace, and the
contentapi module will be disabled (since there would otherwise be a path
conflict between the two).

Note also that there has been no attempt to fork or reproduce the
contentapi_summary, contentapi_taxonomy_term and contentapi_taxonomy_vocab
submodules.
