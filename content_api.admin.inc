<?php
/**
 * @file
 * Contains administrative forms for Content API.
 */

/**
 * Form constructor for the settings form.
 *
 * @param array $form
 *   The form being constructed.
 * @param array $form_state
 *   State of the form being constructed.
 *
 * @return array
 *   The newly-created form.
 */
function content_api_settings(array $form, array &$form_state) {
  $form = array();

  $hidden_fields = variable_get('content_api_node_hidden_fields', '');
  if (!is_array($hidden_fields)) {
    $hidden_fields = array();
  }
  $node_fields = array(
    'created'  => 'created',
    'changed'  => 'changed',
    'title'    => 'title',
    'webUrl'   => 'webUrl',
    'nid'      => 'nid',
    'type'     => 'type',
    'body'     => 'body',
    'vid'      => 'vid',
    'taxonomy' => 'taxonomy',
    'language' => 'language'
  );
  $defaults = array();

  foreach ($node_fields as $field) {
    if (!in_array($field, $hidden_fields)) {
      $defaults[$field] = $field;
    }
  }

  $form['content_api_node_hidden_fields'] = array(
    '#type' => 'checkboxes',
    '#options' => $node_fields,
    '#default_value' => $defaults,
    '#title' => t('Node fields'),
    '#description' => t('Only checked node fields will display in the API.'),
  );

  $form['content_api_limit'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('content_api_limit', 10),
    '#title' => 'Node return limit',
    '#description' => 'Maximum number of nodes to return per API call.',
  );

  $schema = drupal_get_schema('node');
  $form['content_api_sort_field'] = array(
    '#type' => 'select',
    '#default_value' => variable_get('content_api_sort_field', 'created'),
    '#title' => t('Default sort field'),
    '#options' => drupal_map_assoc(array_keys($schema['fields'])),
  );

  $form['content_api_sort_order'] = array(
    '#type' => 'select',
    '#options' => array('ASC' => t('Ascending'), 'DESC' => t('Descending')),
    '#title' => t('Default sort order'),
    '#default_value' => variable_get('content_api_sort_order', 'DESC'),
  );

  $form['content_api_show_count'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show count item as first result'),
    '#description' => t('Check to show an item that includes count, pages, itemsPerPage and currentPage.'),
    '#default_value' => variable_get('content_api_show_count', TRUE)
  );

  $options = array_map('check_plain', node_type_get_names());
  $checked_options = variable_get('content_api_content_types', '');

  $form['content_api_content_types'] = array(
    '#type' => 'checkboxes',
    '#options' => $options,
    '#title' => t('Content types to publish in API'),
    '#description' => t('Select the node types to publish in the API.'),
    '#default_value' => $checked_options,
  );

  return system_settings_form($form);
}

/**
 * Validator for the settings form.
 *
 * @param array $form
 *   The form being validated.
 * @param array $form_state
 *   State of the form being validated.
 */
function content_api_settings_validate(array $form, array &$form_state) {
  $hidden_fields = $form_state['values']['content_api_node_hidden_fields'];

  if ($hidden_fields) {
    $hide_fields = array();
    foreach ($hidden_fields as $field => $show) {
      if ($show === 0) {
        $hide_fields[] = $field;
      }
    }
    $form_state['values']['content_api_node_hidden_fields'] = $hide_fields;
  }
}

function content_api_node_type_display_form(array $form, array &$form_state, $type) {
  $fields = field_info_instances('node', $type);

  $form['type'] = array(
    '#type' => 'hidden',
    '#value' => $type,
  );

  $hidden_fields = variable_get('content_api_type_' . strtolower($type), '');
  if (!is_array($hidden_fields)) {
    $hidden_fields = array();
  }

  $form['fields'] = array(
    '#type' => 'fieldset',
    '#title' => t('Fields'),
  );

  foreach ($fields as $field => $info) {
    // Only show 'field' or 'og' fields.
    if (substr($field, 0, 6) == 'field_' || substr($field, 0, 3) == 'og_') {
      $checked = !in_array($field, $hidden_fields);
      $form['fields'][$field] = array(
        '#title' => check_plain($info['label']),
        '#type' => 'checkbox',
        '#default_value' => $checked,
      );
    }
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit'
  );

  return $form;
}

function content_api_node_type_display_form_submit(array $form, array &$form_state) {
  $hidden_fields = array();
  $name = $form_state['values']['type'];

  foreach ($form_state['values'] as $key => $value) {
    if (substr($key, 0, 6) == 'field_' && $value === 0) {
      $hidden_fields[] = $key;
    }
  }

  variable_set('content_api_type_' . strtolower($name), $hidden_fields);
  drupal_set_message(t('The configuration options have been saved.'));
}
